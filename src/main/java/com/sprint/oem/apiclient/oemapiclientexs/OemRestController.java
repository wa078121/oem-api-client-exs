package com.sprint.oem.apiclient.oemapiclientexs;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sprint.oem.apiclient.model.ImeiRequest;
import com.sprint.oem.apiclient.model.ImeiResponse;
import com.tmobile.security.taap.poptoken.builder.exception.PopTokenBuilderException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class OemRestController {

	@Autowired
	PopTokenGen popTokenbuilder;

	@Autowired
	OauthTokenGen oauthTokenGen;
	
	@Autowired
	PopTokenGenForQuery popTokenGenForQuery;
	
	@Autowired
	ObjectMapper objectMapper;

	@GetMapping("/processimei")
	public void processimei(@RequestParam(value = "imei") String imei, @RequestParam(value = "sku") String sku,
			@RequestParam(value = "rowid") String rowid) throws PopTokenBuilderException, JsonProcessingException {
		 generateIMEIDetails();
		String oauthpoptoken = "";
		try {
			oauthpoptoken = popTokenbuilder.buildPopTokenForOAuthTokenRequest();
		} catch (PopTokenBuilderException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("TEST POPTOKEN:: \n" + "pop:" + oauthpoptoken);
		System.out.println("In OemRestController:processimei >>>>>>>>>>>>");
		System.out.println("TEST:: \n" + "IMEI:" + imei + "\nSKU:" + sku + "\nRowId:" + rowid);

		String access_token = "";
		try {
			access_token = oauthTokenGen.getAccessToken();
		} catch (PopTokenBuilderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		  RestTemplate restTemplate = new RestTemplate(); 
		  String url = "https://qlab02.core.op.api.t-mobile.com:443/devicemanagement/v1/reserve-esim/query-eid";
		  
		  ImeiRequest imeiRequest = new ImeiRequest();
		  imeiRequest.setImei(imei);
		  //imeiRequest.setByodDevice(true);
		  //imeiRequest.setSerialNumber("FDRXXTTYY");
	       // String body = "{\"imei\":354144113833915, \"serialNumber\": \"FDRXXTTYY\", \"byodDevice\": true }";
		  String objBody = objectMapper.writeValueAsString(imeiRequest);
		  System.out.println("##########"+objBody);
	        String body = objBody;
	        		//"{\"imei\":\"354144113833915\" }";
		  HttpHeaders headers = new HttpHeaders();
		  
		  headers.set("Content-Type", "application/json");
		  headers.set("Authorization","Bearer "+ access_token); 
		  headers.set("X-Authorization",popTokenGenForQuery.buildPopTokenForQueryRequest(objBody)); 
		  headers.set("Accept","application/json");
		  
		  HttpEntity<ImeiRequest> requestEntity = new HttpEntity<>(imeiRequest,headers);
		  
		  ResponseEntity<ImeiResponse> responseEntity =
				  restTemplate.exchange(url,HttpMethod.POST, requestEntity, ImeiResponse.class);
		  //ImeiResponse imeiResponse = responseEntity.getBody(); 
		  System.out.println("performFunctionalCallWithPopTokenBySendingOrderEntityAsString -> response: " + responseEntity.getBody());
		 
		  //return imeiResponse.toString();
		 

			/*
			 * fetchMissingImei2Eid(imei, sku, rowid); String imei2 = "", eid2 = "";
			 * updateEXSDbWithMissingImei2Eid(imei2, eid2, sku, rowid);
			 */

			/*
			 * return "TEST:: \n" + "IMEI:" + imei + "\nSKU:" + sku + "\nRowId:" + rowid +
			 * "### ACCESS_TOKEN:::" + access_token;
			 */
	}

	// private generatePopToken() {

	// }
	private void fetchMissingImei2Eid(String imei, String sku, String rowid) {
		// TODO Auto-generated method stub

	}

	private void updateEXSDbWithMissingImei2Eid(String imei2, String eid2, String sku, String rowid) {
		// TODO Auto-generated method stub

	}
	
	private void generateIMEIDetails() {
		
		try
		{
			// initializing FileInputStream
			FileInputStream firstFile = new FileInputStream("C:\\tmp\\hw_200.csv");

			// Initializing InputStreamReader object
			InputStreamReader inputRead = new InputStreamReader(firstFile);
			
			FileWriter out = new FileWriter("C:\\tmp\\hw_200_1.csv");
			CSVPrinter printer = CSVFormat.DEFAULT.withHeader("IMEI", "IMEI2","EID").print(out);
			
			String imei="";
			String imei2="";
			String eid="";
			
			CSVParser csvParser = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(inputRead);
			for (CSVRecord record : csvParser) {
				imei =	record.get("IMEI"); 
				imei2 = "67678676";
				eid="878979887";
				if(imei!=null && !imei.isEmpty() ) {
				printer.printRecord(imei, imei2,eid);
				printer.flush();
				}
				
			}
			
		}catch (FileNotFoundException fnfe){
			System.out.println("NO Such File Exists");
		}
		catch (IOException except){
			System.out.println("IOException occured");
		}
	}
	

}