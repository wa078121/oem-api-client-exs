package com.sprint.oem.apiclient.oemapiclientexs;

import java.util.LinkedHashMap;

import com.tmobile.security.taap.poptoken.builder.PopEhtsKey;
import com.tmobile.security.taap.poptoken.builder.PopTokenBuilder;
import com.tmobile.security.taap.poptoken.builder.exception.PopTokenBuilderException;
import org.springframework.stereotype.Component;

@Component
public class PopTokenGen {

    public String buildPopTokenForOAuthTokenRequest() throws PopTokenBuilderException {

        String privateKeyPemString = "-----BEGIN PRIVATE KEY-----\r\n" +
        		"MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC99tPgPgblZ1w+\r\n" +
        		"6sm6/MWziyPuOPdpmt7ARw20/LuimsdRdBfAVahArdVFWv8xnBZNOHm/TigBZVK3\r\n" +
        		"ZDfnPiXbOJo05fll7ebN7mTzh5sqzX24NGqeNf8RtfMaAD1RzDZS5nGPJx1MxrAw\r\n" +
        		"Izm+e2KDw0Gxdj+ayhjBxnSa52Uoyo0sro9Y7UBOa9ZszXagWSutrW3UrKjG5x22\r\n" +
        		"0Jz+On+TqmNwsBRs8V1xOsV6uhXnjVOs1iXVrq3T2Ca0XhJJCUwTFTmDQW66aWC0\r\n" +
        		"4bcBHtnpcAYSJ021Qp794zRPPvIkaAIRS/j1ySDskznL9IvNMBiK84ujbGVZeVbh\r\n" +
        		"qF5z5VTDAgMBAAECggEBAJDlpgVEqrPu3DDtnAQx7Icf2b9K2oVF6SSWzgDASksk\r\n" +
        		"bmZV/AvsClOThLrLRx5LJcOvEUZ30hIGfEqAehe7ktqdWRqCVHKIPWXUslIDnLrQ\r\n" +
        		"VVXdPXoCS9XxiEXL6vnqn4EqX3JG7pVydE+zfscpjCJfHEvFm4xxHOMpgiXp3IJ4\r\n" +
        		"dAxmIEN+bvs2Cl31JD+eQLaRDnEPa6pyAT7u4SoZsL54/hkGknynCeEQpR6Tk/76\r\n" +
        		"sgnJ/2SSZ2obBq2gz0V41S9Faqp8eUNiH2QLV0kqUHhcjtd7EL1w2tSYG7dpcXOr\r\n" +
        		"iRAgpU+TPxp7OfXvVzHdXpfi38sAsFKNINkEB/I8yOECgYEA7+F5YqZAgsEO+57G\r\n" +
        		"ELWzh9ki+fDewb/lyH4d+CK7eMMSCdBnrOBuMfe3ipIJzbabHD40tlO8+qcIxTRZ\r\n" +
        		"UM/ZTImreUq8x1B8K44OsDlPnCifeGkfJJx3xMNBbvAUncs5hHlZVjleK3spjCeD\r\n" +
        		"r7ZVy8OuSt00QipOgaXgYy+HTDMCgYEAyrqrC09l7Z2khZwYZjHBpU0JeNInCXz0\r\n" +
        		"6R6zzQRzWettEYvQvIsGSdZYgOyBYTm8T7wKl4QbNjoomiwXT2N7ztg1cvlk+ye5\r\n" +
        		"S8Fli5wVEIUaKISQl2IBj2BYHgqYIIwu1xeSqI1vCW1TD6ZO+5k35soz2RPFatu5\r\n" +
        		"+1QvGHiyRTECgYAaAJbn2p2jjsnQ/KT7qraLQz5HWZTL68E8um8FNr6bmImcBrs2\r\n" +
        		"Qh2vzOpYWvw1EFY/+X49RVZ21SRePA5ydBVZ9pJf03ojINZU4lE1SQEYLE8c/kgh\r\n" +
        		"79QLwB6Xp+6ULDNP6NeGQTG0BOW8C4x6v4kNTWjXNKM1iOB5rUFlfwFrgQKBgQCF\r\n" +
        		"coMNGuK5mz+lQ6879hzGNwO5KHzR3hNJMRCiL//OfQ56slJhChLFn/b6RrSTz3GJ\r\n" +
        		"+vnt6uQOuhHoCoX9mDrzWZc1a71xJiDvIQs67y9ehH9HlJupSrZELuNSQx8Irb5X\r\n" +
        		"kAjWMuOYjQJUFuF+9Mi7fvbKOh2FzcWLD0tBDJIswQKBgQDkcDVso+yvvYC1EUsL\r\n" +
        		"SLJ7iV5ft/wHuA6Bm3nc9Cz8Q/BiwVmHR9ilUCVGOoaCm+XXTZ7Jcvb6ucooCTja\r\n" +
        		"n7+tLMJYzQfJdFCaxK5r6EzP15SfHIIVrfD/3+GGXHhN3F7zt86NvEH5HiFaiWDw\r\n" +
        		"kI714w/SLZ38nb8TGcKsReCg+Q==\r\n" +
        		"-----END PRIVATE KEY-----\r\n";

        // STEP 1: Build the LinkedHashMap of ehts (external headers to sign) key and values
        LinkedHashMap<String, String> ehtsKeyValueMap = new LinkedHashMap<>();
        ehtsKeyValueMap.put("Authorization", "Basic Ykh1WmkzME9FUDR4ajZQeXFHdkRVcFVpRGJxTERiVWQ6S0dSOVVNd1IzMDNpWlRXbA==");
        ehtsKeyValueMap.put(PopEhtsKey.URI.keyName(), "/oauth2/v6/tokens");
        ehtsKeyValueMap.put(PopEhtsKey.HTTP_METHOD.keyName(), "POST");
        ehtsKeyValueMap.put("grant-type", "client_credentials");

        // STEP 2: Generate PoP token using PoPTokenBuilder
        String popToken = PopTokenBuilder.newInstance() //
                .setEhtsKeyValueMap(ehtsKeyValueMap) //
                .signWith(privateKeyPemString) //
                .build();

        return popToken;
    }

 
}
