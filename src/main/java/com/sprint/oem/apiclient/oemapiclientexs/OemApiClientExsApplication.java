package com.sprint.oem.apiclient.oemapiclientexs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OemApiClientExsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OemApiClientExsApplication.class, args);
		System.out.println("OemApiClientExsApplication:main >>>>>>>>>>");
	}

}
