package com.sprint.oem.apiclient.oemapiclientexs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.tmobile.security.taap.poptoken.builder.exception.PopTokenBuilderException;
@Component
public class OauthTokenGen {
	
	@Autowired
	PopTokenGen popTokenGen;
	
	public String getAccessToken() throws PopTokenBuilderException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://qlab02.core.op.api.t-mobile.com:443/oauth2/v6/tokens";

        HttpHeaders headers = new HttpHeaders();
        headers.set("grant-type", "client_credentials");

        // For clientId/secret => PoPDevApp/PoPDevAppSecret
        headers.set("Authorization", "Basic Ykh1WmkzME9FUDR4ajZQeXFHdkRVcFVpRGJxTERiVWQ6S0dSOVVNd1IzMDNpWlRXbA==");
        headers.set("X-Authorization", popTokenGen.buildPopTokenForOAuthTokenRequest());
        headers.set("Accept", "application/json");

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);
        ResponseEntity<TokenDetails> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, TokenDetails.class);
        TokenDetails tokenDetails = responseEntity.getBody();
        return tokenDetails.getAccessToken();
    }
}
