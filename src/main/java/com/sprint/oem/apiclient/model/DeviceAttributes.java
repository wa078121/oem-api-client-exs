package com.sprint.oem.apiclient.model;

public class DeviceAttributes {
    	
	private String carrier;
	private String sku;
	private String meidHex;
	private String meidDec;
	private String manufacturedDate;
	private String modelCode;
	
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getMeidHex() {
		return meidHex;
	}
	public void setMeidHex(String meidHex) {
		this.meidHex = meidHex;
	}
	public String getMeidDec() {
		return meidDec;
	}
	public void setMeidDec(String meidDec) {
		this.meidDec = meidDec;
	}
	public String getManufacturedDate() {
		return manufacturedDate;
	}
	public void setManufacturedDate(String manufacturedDate) {
		this.manufacturedDate = manufacturedDate;
	}
	public String getModelCode() {
		return modelCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}
    	
}
