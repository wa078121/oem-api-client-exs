package com.sprint.oem.apiclient.model;

public class ImeiResponse {
	
	private String eidCsn;
	private boolean byodDevice;
	private Long imei;
	private String[] secondaryImei;
	private String manufacturer;
	private String deviceType;
	private String model;
	private DeviceAttributes deviceAttributes;
	
	public String getEidCsn() {
		return eidCsn;
	}
	public void setEidCsn(String eidCsn) {
		this.eidCsn = eidCsn;
	}
	public boolean isByodDevice() {
		return byodDevice;
	}
	public void setByodDevice(boolean byodDevice) {
		this.byodDevice = byodDevice;
	}
	public Long getImei() {
		return imei;
	}
	public void setImei(Long imei) {
		this.imei = imei;
	}
	public String[] getSecondaryImei() {
		return secondaryImei;
	}
	public void setSecondaryImei(String[] secondaryImei) {
		this.secondaryImei = secondaryImei;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public DeviceAttributes getDeviceAttributes() {
		return deviceAttributes;
	}
	public void setDeviceAttributes(DeviceAttributes deviceAttributes) {
		this.deviceAttributes = deviceAttributes;
	}
}
