package com.sprint.oem.apiclient.model;

public class ImeiRequest {
	
	//private String serialNumber;
	private String imei;
	//private boolean byodDevice;
	
	/*
	 * public String getSerialNumber() { return serialNumber; } public void
	 * setSerialNumber(String serialNumber) { this.serialNumber = serialNumber; }
	 */
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	/*
	 * public boolean isByodDevice() { return byodDevice; } public void
	 * setByodDevice(boolean byodDevice) { this.byodDevice = byodDevice; }
	 */
}
